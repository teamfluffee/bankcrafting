package scripts.modules.bankcrafting;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;
import scripts.modules.bankcrafting.data.Methods;
import scripts.modules.bankcrafting.data.items.Leather;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Sub Scripts",
        name        = "Bank Crafting",
        description = "It's like other crafting, but with way less walking.",
        gameMode = 1)

public class BankCraftingScript extends MissionScript implements Painting {

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#ff0054"), "Bank Crafting")
            .addField("Version", Double.toString(1.00))
            .build();

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public Mission getMission() {
        return new BankCrafting(Methods.LEATHER_CRAFTING, Leather.COIF);
    }
}
