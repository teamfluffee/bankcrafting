package scripts.modules.bankcrafting.data.items;

import scripts.modules.bankcrafting.data.Methods;
import scripts.modules.bankcrafting.data.materials.LeatherMaterials;
import scripts.modules.bankcrafting.data.materials.ShieldMaterials;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

public enum Dragonhide implements CraftingItem {

    GREEN_DHIDE_VAMB(
            57,
            62,
            new Interactable("Green d'hide vamb", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.GREEN_DRAGON_LEATHER, 1)
    ),
    GREEN_DHIDE_CHAPS(
            60,
            124,
            new Interactable("Green d'hide chaps", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.GREEN_DRAGON_LEATHER, 2)
    ),
    GREEN_DHIDE_SHIELD(
            62,
            124,
            new Interactable("Green d'hide shield", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.GREEN_DRAGON_LEATHER, 2),
            makeMaterial(ShieldMaterials.MAPLE_SHIELD, 1),
            makeMaterial(ShieldMaterials.STEEL_NAILS, 15)
    ),
    GREEN_DHIDE_BODY(
            63,
            186,
            new Interactable("Green d'hide body", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.GREEN_DRAGON_LEATHER, 3)
    ),
    BLUE_DHIDE_VAMB(
            57,
            62,
            new Interactable("Blue d'hide vamb", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLUE_DRAGON_LEATHER, 1)
    ),
    BLUE_DHIDE_CHAPS(
            60,
            124,
            new Interactable("Blue d'hide chaps", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLUE_DRAGON_LEATHER, 2)
    ),
    BLUE_DHIDE_SHIELD(
            62,
            124,
            new Interactable("Blue d'hide shield", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLUE_DRAGON_LEATHER, 2),
            makeMaterial(ShieldMaterials.YEW_SHIELD, 1),
            makeMaterial(ShieldMaterials.MITHRIL_NAILS, 15)
    ),
    BLUE_DHIDE_BODY(
            63,
            186,
            new Interactable("Blue d'hide body", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLUE_DRAGON_LEATHER, 3)
    ),
    RED_DHIDE_VAMB(
            57,
            62,
            new Interactable("Red d'hide vamb", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.RED_DRAGON_LEATHER, 1)
    ),
    RED_DHIDE_CHAPS(
            60,
            124,
            new Interactable("Red d'hide chaps", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.RED_DRAGON_LEATHER, 2)
    ),
    RED_DHIDE_SHIELD(
            62,
            124,
            new Interactable("Red d'hide shield", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.RED_DRAGON_LEATHER, 2),
            makeMaterial(ShieldMaterials.MAGIC_SHIELD, 1),
            makeMaterial(ShieldMaterials.ADAMANT_NAILS, 15)
    ),
    RED_DHIDE_BODY(
            63,
            186,
            new Interactable("Red d'hide body", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.RED_DRAGON_LEATHER, 3)
    ),
    BLACK_DHIDE_VAMB(
            57,
            62,
            new Interactable("Black d'hide vamb", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLACK_DRAGON_LEATHER, 1)
    ),
    BLACK_DHIDE_CHAPS(
            60,
            124,
            new Interactable("Black d'hide chaps", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLACK_DRAGON_LEATHER, 2)
    ),
    BLACK_DHIDE_SHIELD(
            62,
            124,
            new Interactable("Black d'hide shield", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLACK_DRAGON_LEATHER, 2),
            makeMaterial(ShieldMaterials.REDWOOD_SHIELD, 1),
            makeMaterial(ShieldMaterials.RUNE_NAILS, 15)
    ),
    BLACK_DHIDE_BODY(
            63,
            186,
            new Interactable("Black d'hide body", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.BLACK_DRAGON_LEATHER, 3)
    );

    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Dragonhide(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.LEATHER_CRAFTING;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}
