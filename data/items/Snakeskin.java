package scripts.modules.bankcrafting.data.items;

import scripts.modules.bankcrafting.data.Methods;
import scripts.modules.bankcrafting.data.materials.LeatherMaterials;
import scripts.modules.bankcrafting.data.materials.ShieldMaterials;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

public enum Snakeskin implements CraftingItem {

    SNAKESKIN_BOOTS(
            45,
            30,
            new Interactable("Snakeskin boots", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 6)
    ),
    SNAKESKIN_VAMBRACES(
            47,
            35,
            new Interactable("Snakeskin vambraces", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 8)
    ),
    SNAKESKIN_BANDANA(
            48,
            45,
            new Interactable("Snakeskin bandana", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 5)
    ),
    SNAKESKIN_CHAPS(
            51,
            50,
            new Interactable("Snakeskin chaps", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 12)
    ),
    SNAKESKIN_BODY(
            53,
            55,
            new Interactable("Snakeskin body", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 15)
    ),
    SNAKESKIN_SHIELD(
            56,
            100,
            new Interactable("Snakeskin shield", "Wear", DEFAULT_ID),
            makeMaterial(LeatherMaterials.SNAKESKIN, 2),
            makeMaterial(ShieldMaterials.WILLOW_SHIELD, 1),
            makeMaterial(ShieldMaterials.IRON_NAILS, 15)
    );

    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Snakeskin(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return 0;
    }

    @Override
    public double getXpGained() {
        return 0;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.LEATHER_CRAFTING;
    }

    @Override
    public Interactable getProduct() {
        return null;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return new InteractableItem[0];
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
        return CraftingItem.makeMaterial(material, quantity);
    }
}
