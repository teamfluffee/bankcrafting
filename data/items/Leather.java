package scripts.modules.bankcrafting.data.items;

import scripts.modules.bankcrafting.data.Methods;
import scripts.modules.bankcrafting.data.materials.LeatherMaterials;
import scripts.modules.bankcrafting.data.materials.ShieldMaterials;
import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;

@SuppressWarnings("unused")
public enum Leather implements CraftingItem {
    LEATHER_GLOVES(
            1,
            13.75,
            new Interactable("Leather gloves", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    LEATHER_BOOTS(
            7,
            16.25,
            new Interactable("Leather boots", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    LEATHER_COWL(
            9,
            18.5,
            new Interactable("Leather cowl", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    LEATHER_VAMBRACES(
            11,
            22,
            new Interactable("Leather vambraces", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    LEATHER_BODY(
            14,
            25,
            new Interactable("Leather body", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    LEATHER_CHAPS(
            18,
            27,
            new Interactable("Leather chaps", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    HARD_LEATHER_BODY(
            28,
            35,
            new Interactable("Hardleather body", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.HARD_LEATHER, 1)
    ),
    COIF(
            38,
            37,
            new Interactable("Coif", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.SOFT_LEATHER, 1)
    ),
    HARD_LEATHER_SHIELD(
            41,
            70,
            new Interactable("Hardleather shield", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.HARD_LEATHER, 2),
            makeMaterial(ShieldMaterials.OAK_SHIELD, 1),
            makeMaterial(ShieldMaterials.BRONZE_NAILS, 15)
    ),
    STUDDED_BODY(
            41,
            40,
            new Interactable("Studded body", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.HARD_LEATHER, 1)
    ),
    STUDDED_CHAPS(
            44,
            42,
            new Interactable("Studded chaps", "Wear", Interactable.DEFAULT_ID),
            makeMaterial(LeatherMaterials.HARD_LEATHER, 1),
            makeMaterial(LeatherMaterials.STEEL_STUDS, 1)
    );

    private int levelRequired;
    private double xpGained;
    private Interactable product;
    private InteractableItem[] materials;

    Leather(int levelRequired, double xpGained, Interactable product, InteractableItem... materials) {
        this.levelRequired = levelRequired;
        this.xpGained = xpGained;
        this.product = product;
        this.materials = materials;
    }

    @Override
    public int getLevelRequired() {
        return levelRequired;
    }

    @Override
    public double getXpGained() {
        return xpGained;
    }

    @Override
    public CraftingMethods getMethod() {
        return Methods.LEATHER_CRAFTING;
    }

    @Override
    public Interactable getProduct() {
        return product;
    }

    @Override
    public InteractableItem[] getMaterials() {
        return materials;
    }

    public static InteractableItem makeMaterial(CraftingMaterial material, int quantity) {
       return CraftingItem.makeMaterial(material, quantity);
    }
}
