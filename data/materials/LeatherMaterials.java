package scripts.modules.bankcrafting.data.materials;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

public enum LeatherMaterials implements CraftingMaterial {

    SOFT_LEATHER(new Interactable("LEATHER", "Use", DEFAULT_ID)),
    HARD_LEATHER(new Interactable("Hard leather", "Use", DEFAULT_ID)),
    GREEN_DRAGON_LEATHER(new Interactable("Green dragon leather", "Use", DEFAULT_ID)),
    BLUE_DRAGON_LEATHER(new Interactable("Blue dragon leather", "Use", DEFAULT_ID)),
    RED_DRAGON_LEATHER(new Interactable("Red dragon leather", "Use", DEFAULT_ID)),
    BLACK_DRAGON_LEATHER(new Interactable("Black dragon leather", "Use", DEFAULT_ID)),
    SNAKESKIN(new Interactable("SNAKESKIN", "Use", DEFAULT_ID)),

    STEEL_STUDS(new Interactable("Steel studs", "Use", DEFAULT_ID));

    private final Interactable material;

    LeatherMaterials(Interactable material) {
        this.material = material;
    }

    @Override
    public Interactable getMaterial() {
        return material;
    }
}
