package scripts.modules.bankcrafting.data.materials;

import scripts.fluffeesapi.data.interactables.Interactable;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMaterial;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

public enum ShieldMaterials implements CraftingMaterial {

    OAK_SHIELD(new Interactable("Oak shield", "Use", DEFAULT_ID)),
    WILLOW_SHIELD(new Interactable("Willow shiled", "Use", DEFAULT_ID)),
    MAPLE_SHIELD(new Interactable("Maple shield", "Use", DEFAULT_ID)),
    YEW_SHIELD(new Interactable("Yew shield", "Use", DEFAULT_ID)),
    MAGIC_SHIELD(new Interactable("Magic shield", "Use", DEFAULT_ID)),
    REDWOOD_SHIELD(new Interactable("Redwood shield", "Use", DEFAULT_ID)),

    BRONZE_NAILS(new Interactable("Bronze nails", "Use", DEFAULT_ID)),
    IRON_NAILS(new Interactable("Iron nails", "Use", DEFAULT_ID)),
    STEEL_NAILS(new Interactable("Steel nails", "Use", DEFAULT_ID)),
    MITHRIL_NAILS(new Interactable("Mithril nails", "Use", DEFAULT_ID)),
    ADAMANT_NAILS(new Interactable("Adamant nails", "Use", DEFAULT_ID)),
    RUNE_NAILS(new Interactable("Rune nails", "Use", DEFAULT_ID));

    private final Interactable material;

    ShieldMaterials(Interactable material) {
        this.material = material;
    }

    @Override
    public Interactable getMaterial() {
        return null;
    }
}
