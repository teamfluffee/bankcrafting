package scripts.modules.bankcrafting.data;

import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.data.structures.AntibanVariables;

import java.util.concurrent.atomic.AtomicInteger;

public class Variables extends AntibanVariables {

    private Methods craftingMethod;
    private CraftingItem craftingItem;

    public Variables(AtomicInteger numberSleeps, AtomicInteger averageSleepTime,
                     Methods craftingMethod, CraftingItem craftingItem) throws IllegalAccessException {
        super(numberSleeps, averageSleepTime);
        this.craftingMethod = craftingMethod;
        this.craftingItem = craftingItem;
    }

    public Methods getCraftingMethod() {
        return craftingMethod;
    }

    public CraftingItem getCraftingItem() {
        return craftingItem;
    }
}
