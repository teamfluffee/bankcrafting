package scripts.modules.bankcrafting.data;

import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.data.skilling.crafting.CraftingMethods;

import static scripts.fluffeesapi.data.interactables.Interactable.DEFAULT_ID;

@SuppressWarnings("unused")
public enum Methods implements CraftingMethods {

    LEATHER_CRAFTING(new InteractableItem("Needle", "Use", DEFAULT_ID, 1),
            new InteractableItem("Thread", "Use", DEFAULT_ID, 7)),
    STUDDED_CRAFTING(new InteractableItem("Needle", "Use", DEFAULT_ID, 1),
            new InteractableItem("Thread", "Use", DEFAULT_ID, 7),
            new InteractableItem("Steel studs", "Use", DEFAULT_ID, 25)
    ),
    SHIELD_CRAFTING(new InteractableItem("Hammer", "Use", DEFAULT_ID, 1));

    private InteractableItem[] craftingEquipment;

    Methods(InteractableItem... craftingEquipment) {
        this.craftingEquipment = craftingEquipment;
    }

    @Override
    public InteractableItem[] getCraftingEquipment() {
        return craftingEquipment;
    }
}
