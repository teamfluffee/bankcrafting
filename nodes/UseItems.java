package scripts.modules.bankcrafting.nodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.types.RSItem;
import scripts.modules.bankcrafting.data.Variables;
import scripts.fluffeesapi.client.clientextensions.Game;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.interactables.InteractableItem;
import scripts.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.fluffeesapi.scripting.frameworks.task.tasktypes.SuccessTask;
import scripts.fluffeesapi.utilities.MakeAllUtil;

public class UseItems extends SuccessTask {

    private Variables variables;

    public UseItems(Variables variables) {
        this.variables = variables;
    }

    @Override
    public Priority priority() {
        return Priority.HIGH;
    }

    @Override
    public boolean isValid() {
        return !MakeAllUtil.isMakeAllOpen();
    }

    @Override
    public void successExecute() {
        InteractableItem[] craftingEquipment = variables.getCraftingMethod().getCraftingEquipment();
        InteractableItem[] craftingMaterials = variables.getCraftingItem().getMaterials();
        if (craftingEquipment.length == 0 || craftingMaterials.length == 0 || !GameTab.open(GameTab.TABS.INVENTORY)) {
            return;
        }

        if (Game.getItemSelectionState() == 1 && Game.getSelectedItemName().equals(craftingEquipment[0].getName())) {
            RSItem equipment = Inventory.findFirst(craftingMaterials[1]);
            if (equipment == null) {
                return;
            }

            Clicking.click("Use", equipment);
            Timing.waitCondition(() -> {
                General.sleep(200, 400);
                return MakeAllUtil.isMakeAllOpen();
            }, General.random(3000, 5000));
        } else {
            RSItem equipment = Inventory.findFirst(craftingEquipment[0]);
            if (equipment == null) {
                return;
            }

            Clicking.click("Use", equipment);
            Timing.waitCondition(() -> {
                General.sleep(200, 400);
                return Game.getItemSelectionState() == 1;
            }, General.random(3000, 5000));
        }
    }

    @Override
    public String getStatus() {
        return "Using items";
    }
}
