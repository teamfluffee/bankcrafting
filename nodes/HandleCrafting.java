package scripts.modules.bankcrafting.nodes;

import org.tribot.api2007.Skills;
import scripts.modules.bankcrafting.data.Variables;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.scripting.frameworks.task.Priority;
import scripts.fluffeesapi.scripting.frameworks.task.tasktypes.AntibanTask;
import scripts.fluffeesapi.utilities.MakeAllUtil;

public class HandleCrafting extends AntibanTask {

    private int startingLevel, targetXp;

    public HandleCrafting(Variables variables) {
        super(variables);
    }

    @Override
    public Priority priority() {
        return Priority.LOW;
    }

    @Override
    public boolean isValid() {
        return MakeAllUtil.isMakeAllOpen();
    }

    @Override
    public void preActivitySetup() {
        startingLevel = Skills.getCurrentLevel(Skills.SKILLS.CRAFTING);
        targetXp = (int) (Skills.getXP(Skills.SKILLS.CRAFTING) + ((Variables) variables).getCraftingItem().getXpGained()
                        * Inventory.getCount(((Variables) variables).getCraftingItem().getMaterials()[0]));
    }

    @Override
    public boolean getStoppingCondition() {
        return Skills.getCurrentLevel(Skills.SKILLS.CRAFTING) > startingLevel ||
                Skills.getXP(Skills.SKILLS.CRAFTING) >= targetXp;
    }

    @Override
    public boolean beginAntibanActivity() {
        return MakeAllUtil.makeItems(((Variables) variables).getCraftingItem().getProduct().getName());
    }

    @Override
    public String getStatus() {
        return "Crafting";
    }
}
