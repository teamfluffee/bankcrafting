package scripts.modules.bankcrafting;

import org.tribot.api2007.Skills;
import scripts.modules.bankcrafting.data.Methods;
import scripts.modules.bankcrafting.data.Variables;
import scripts.modules.bankcrafting.nodes.HandleCrafting;
import scripts.modules.bankcrafting.nodes.UseItems;
import scripts.fluffeesapi.client.clientextensions.Inventory;
import scripts.fluffeesapi.data.skilling.crafting.CraftingItem;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.taskmissions.TaskMission;
import scripts.fluffeesapi.scripting.frameworks.task.TaskSet;

import java.util.concurrent.atomic.AtomicInteger;

public class BankCrafting implements TaskMission {

    private Variables variables;

    public BankCrafting(AtomicInteger numberSleeps, AtomicInteger averageAntibanSleep,
                        Methods craftingMethod, CraftingItem craftingItem) {
        try {
            this.variables = new Variables(numberSleeps, averageAntibanSleep, craftingMethod, craftingItem);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TaskSet getTaskSet() {
        return new TaskSet(
                new UseItems(variables),
                new HandleCrafting(variables)
        );
    }

    @Override
    public String getMissionName() {
        return "Bank Crafting";
    }

    @Override
    public boolean isMissionValid() {
        return variables.getCraftingItem().getLevelRequired() <= Skills.getActualLevel(Skills.SKILLS.CRAFTING) &&
                Inventory.inventoryContainsAll(variables.getCraftingMethod().getCraftingEquipment()) &&
                Inventory.inventoryContainsAll(variables.getCraftingItem().getMaterials());
    }

    @Override
    public boolean isMissionCompleted() {
        return !isMissionValid();
    }
}
